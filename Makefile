JOB_ENV?=DEV
ifdef CI_COMMIT_TAG
	JOB_ENV:=PROD
else ifeq ($(CI_COMMIT_REF_NAME), master)
	JOB_ENV:=QA
endif

export JOB_ENV

init:
	@echo "Init step"
	@echo $$CI_COMMIT_REF_NAME
	@./scripts/appci.sh init_build

build:
	@echo "Build step"
	@./scripts/appci.sh build_image

test:
	@echo "Test step"
	@./scripts/appci.sh test

#deploy:
#	@echo "Deploy step"
#	@./scripts/appci.sh deploy_app

release:
	@echo "Release step"
	@./scripts/appci.sh release_image
