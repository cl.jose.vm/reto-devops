#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH=$(dirname "$(readlink -f "$BASH_SOURCE")")


export TEST_IMAGE=$(eval echo "\$${JOB_ENV}_reto_devops"):$CI_COMMIT_REF_NAME
export IMAGE=$(eval echo "\$${JOB_ENV}_reto_devops"):$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA
export RELEASE_IMAGE=$(eval echo "\$${JOB_ENV}_reto_devops"):latest
if [ $JOB_ENV == 'PROD'  ]; then
    export IMAGE=$(eval echo "\$${JOB_ENV}_reto_devops"):$CI_COMMIT_TAG
    export TAG_IMAGE=$(eval echo "\$${JOB_ENV}_reto_devops"):$CI_COMMIT_TAG
fi


function release_image {
    echo "Time to release our image to ECR"
    docker pull $TAG_IMAGE
    docker tag $TAG_IMAGE $RELEASE_IMAGE
    docker push $RELEASE_IMAGE
}

function build_image () {

    echo "Time to build and deploy our image to ECR"
    echo $JOB_ENV
    docker build --pull -t $IMAGE .
    docker push $IMAGE
    docker images
}

function test () {

    echo "Time to test"
    echo $JOB_ENV
    npm install
    npm test
}

function init_build () {
    echo ${JOB_ENV}
    echo ${CI_COMMIT_REF_NAME}
    echo "usuario: ${USER_DOCKERHUB} ${TOKEN_DOCKERHUB}"
    #$(docker login --username ${USER_DOCKERHUB} -p  ${TOKEN_DOCKERHUB})
    echo "$TOKEN_DOCKERHUB" | docker login --username $USER_DOCKERHUB --password-stdin
}

$1
