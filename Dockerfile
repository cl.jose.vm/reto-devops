FROM node:alpine3.13
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./
#USER node
RUN npm install
COPY --chown=node:node . .
EXPOSE 3000
CMD [ "node", "index.js" ]
#docker build -t joseviera92/reto-devops .
#docker login
#docker run --name reto-devops -p 3000:3000 -d joseviera92/reto-devops
#docker push joseviera92/reto-devops


