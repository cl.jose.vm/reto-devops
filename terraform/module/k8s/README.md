## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | >= 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | >= 2.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_role.role-retodevops](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role_binding.retodevopsrolesbinding](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_retodevopsrolesbinding"></a> [retodevopsrolesbinding](#input\_retodevopsrolesbinding) | n/a | `any` | n/a | yes |
| <a name="input_retodevopsuser"></a> [retodevopsuser](#input\_retodevopsuser) | n/a | `any` | n/a | yes |
| <a name="input_role-retodevops"></a> [role-retodevops](#input\_role-retodevops) | n/a | `any` | n/a | yes |

## Outputs

No outputs.
