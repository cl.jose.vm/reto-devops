
terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}
provider "kubernetes" {
  #config_path = "~/.kube/config" En el archivo .env creo un config_path el cual es exporto por consola y acceder al kubernetes
}

resource "kubernetes_role" "role-retodevops" {
  metadata {
    name      = var.role-retodevops
    namespace = "default"
    labels = {
      role = "role-retodevops"
    }
  }

  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources  = ["pods"]
    verbs = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "retodevopsrolesbinding" {
  metadata {
    name      = var.retodevopsrolesbinding
    namespace = "default"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = var.role-retodevops
  }

  subject {
    kind      = "User"
    name      = var.retodevopsuser
    namespace = "default"
  }
}