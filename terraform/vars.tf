variable "retodevopsrolesbinding" {
    default = "retodevopsrolesbinding"
}

variable "role-retodevops" {
    default = "role-retodevops"
}

variable "retodevopsuser" {
    default = "retodevopsuser"
}
