# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

¡¡Te damos la bienvenida al desafío de CLM Consultores!! Si estás en este proceso, es porque nos interesa que puedas ser parte de nuestro equipo.
## Fase del proceso de selección:
Antes de comenzar con el desafío, te recomendamos leer las siguientes instrucciones:
  1. Es importante que realices el reto en forma tranquila (tendrás 3 días máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  2. En caso de que cumplas con el perfil técnico del cargo, la segunda fase del proceso de selección es una entrevista técnica, en dónde validaremos tus conocimientos y podrás saber con mayor detalle las funciones asociadas al cargo y sobre el equipo del área.
  3. Si continúas avanzando con nosotros, el próximo paso es una entrevista Psicolaboral con el área de Gestión de Personas y posteriormente la coordinación del ingreso a la empresa.

Una vez completado, no olvide notificar la solución **a Carla Santiago csantiago@clmconsultores.com**

Si tienes alguna duda, puedes escribir a Carla Santiago o enviar un correo a Gestión de Personas rrhh@clmconsultores.com

¡Te deseamos mucho éxito!

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/clm-public/reto-devops.git
Cloning into 'reto-devops'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd ./reto-devops

```
### Instalar Dependencias
```bash
$ npm install
npm WARN basicservice@1.0.0 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.2 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 530 packages from 308 contributors and audited 1203947 packages in 34.589s

21 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/sum.test.js
 PASS  tests/string.test.js

Test Suites: 2 passed, 2 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        5.655s
Ran all test suites.
```

### Ejecutar la app
```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

**NT:** En nuestro equipo consideramos que cada aplicación debe venir bien documentada por parte del desarrollador para que el equipo de **DevOps** puede realizar los procesos de automatización de una manera mas eficaz.

## El reto comienza aquí
Tienes que hacer un **fork** de este repositorio para completar los siguientes retos en tu propia cuenta de `gitlab`. **Siéntete libre de resolver el reto que desees.** La cantidad de retos resueltos nos va a permitir valorar tus habilidades y realizar una **oferta en base a las mismas**.

1. Una vez completado, no olvide notificar la solución al **Carla Santiago (csantiago@clmconsultores.com).**
2. **La solución debe venir bien documentada, ten en cuenta que vamos a ejecutar la solución que nos envies para realizar la evaluación**
3. **Tiempo de solución 3 días**

Si tiene alguna duda, adelante, [abre un issue](https://gitlab.com/clm-public/reto-devops/issues) para hacer cualquier pregunta sobre cualquier reto.

### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

¿Qué pasa con los contenedores? En este momento **(2021)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJS)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http, añace:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-CI** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como Minikube o Microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJS

### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

### Reto 6. Terraform
![docker](./img/tf.png)

En estos días en IaC no se habla de más nada que no sea terraform, en **CLM** ya nos encontramos con pipeline automatizados de Iac. El reto consiste en crear un modulo de terraform que nos permita crear un **rbac.authorization de tipo Role** que solo nos permita ver los pods de nuestro **namespace donde se encuentra al app Nodejs**

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Ya que hoy en día no queremos recordar recetas ni comandos, el reto consiste en **automatizar los retos en un Makefile**, considera especificar cuales son las dependencias necesarias para que tu Makefile se ejecute sin problemas.

**NT:** Se evaluará el orden en el cual se encuentre el repositorio, en el gran universo de aplicaciones que existe hoy en día el orden es un factor importante.


## COMENTARIOS CLAUDIO VIERA
### 1er reto 
que consto de crear un archivo dockerfile con la imagen de Nodejes exponiendo los puertos indicados una vez terminado el docker se subio la imagen docker hub con una cuenta creada, la misma imagen se publico en publico de manera que se pueda descargar la imagen sin problemas. El dockfile se encuentra en la carpeta raiz de reto-devops.

### 2do reto 
que consto de crear un docker-compose se utilizo la imagen que se creo del dockerfile desde docker hub, tambien para este ejercicios se crearon certificados autofirmados con los comandos openssl para asi habilitar el HTTPS, editando el config.d para asi tambien redireccionar las peteciones del puerto 80 al 443, tambien se tuvo que instalar el apache, esto debido para poder crear el HTPASSWD para la auth basic, agregando el bin a mi variables de enterno y ejecutar el htpasswd.exe, cabe destacar que en esto tiempo me dio chance de investigar cada una de las funciones, recursos o documentos para cumplir con el ejercicio.

### 3er reto 
consto de crear un CI/CD para esta se utilizo  GITLAB  se creo un CI/CD para construir la imagen de la aplicacion NodeJS se utilizo makefile para ejecutar los scripts por tema de tiempo no pude completarlo para terraform. ejercicio creado en carpeta raiz

### 4er reto 
consto de un yaml de kubernete utilizando caracteristicas mencionadas en los anteriores retos, a diferencia de otras estructuras tambien se agrego el autoscaling, el ejercicio fue creado en la carpeta K8s para esto se trabajo en window 10 con las herramientas y dashboard que facilita el docker, incluso para levantar un kubernetes

### 5er reto 
Este reto fue el que mas me consto, no lo habia trabajado, pero durante el dia entendi la logica y comence desde 0, utilizando las herramientas para debug e ir explorando los errores o logs e ir levantando cada templent o yaml, sin embargo por falta de tiempo lo hubise podido estudiar mas facilmente para asi tenerlo un poco mejor e usar las notes o helpers o saber realmente montar volmen o archivos e incluso estudiar ciertas funciones. Ejercicios en la carpeta mycharth2

### 6to reto
Se trabajo con terraform para acceder al kubernet se creo un .env el cual contiene una variable del path de kube_config para acceder a los recursos, luego se creo los modulos de terraform y el RBAC

### 7to reto
Por temas de tiempo no puede realizarlo.

